<?php

declare(strict_types=1);

namespace Imajim\EtransactionPluginBundle;

use Payum\Skeleton\Action\AuthorizeAction;
use Payum\Skeleton\Action\CancelAction;
use Payum\Skeleton\Action\ConvertPaymentAction;
use Payum\Skeleton\Action\CaptureAction;
use Payum\Skeleton\Action\NotifyAction;
use Payum\Skeleton\Action\RefundAction;
use Payum\Skeleton\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class EtransactionPaymentGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => 'etransactions',
            'payum.factory_title' => 'E-transactions',
            'payum.action.capture' => new CaptureAction(),
            'payum.action.authorize' => new AuthorizeAction(),
            'payum.action.refund' => new RefundAction(),
            'payum.action.cancel' => new CancelAction(),
            'payum.action.notify' => new NotifyAction(),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
        ]);

        if (false == $config['payum.api']) {
            $config['payum.default_options'] = array(
                'sandbox' => true,
            );
            $config->defaults($config['payum.default_options']);
            $config['payum.required_options'] = [];
            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);
                return new Api((array) $config,
                    $config['payum.http_client'],
                    $config['httplug.message_factory']);
            };
        }
    }
}

