<?php

declare(strict_types=1);

namespace Imajim\EtransactionPluginBundle;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class ImajimEtransactionsPluginBundle extends Bundle
{
    use SyliusPluginTrait;
}
