<?php

declare(strict_types=1);

namespace Imajim\EtransactionPluginBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

final class EtransactionConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identifiant', TextType::class, [
                'label' => 'Identifiant boutique : ',
            ])
            ->add('rang', TextType::class, [
                'label' => 'Rang boutique : ',
            ])
            ->add('site', TextType::class, [
                'label' => 'Site boutique : ',
            ])
            ->add('certificatModeTest', TextType::class, [
                'label' => 'Certificat en mode test : ',
                'required' => false,
            ])
            ->add('certificatModeProd', TextType::class, [
                'label' => 'Certificat en mode production : ',
                'required' => false,
            ])
            ->add('environment', ChoiceType::class, [
                'label' => 'Mode : ',
                'choices' => [
                    'Test' => 'test',
                    'Production' => 'production',
                ],
                'multiple' => false,
                'expanded' => false
            ])
            ->add('hash', ChoiceType::class, [
                'label' => 'Algorithme de signature : ',
                'choices' => [
                    'SH1' => 'SH1',
                    'SHA512' => 'SHA512',
                ],
                'multiple' => false,
                'expanded' => false
            ])
            ->add('urlPagePaiement', UrlType::class, [
                'label' => 'URL de la page de paiement : ',
                'required' => false,
            ])
            ->add('langue', ChoiceType::class, [
                'label' => 'Lanque par défaut : ',
                'choices' => [
                    'Francais' => 'fr',
                ],
                'multiple' => false,
                'expanded' => false
            ])
            ->add('modeValidation', ChoiceType::class, [
                'label' => 'Mode de validation : ',
                'choices' => [
                    'Automatique' => 'auto',
                    'Manuelle' => 'manuelle',
                ],
                'multiple' => false,
                'expanded' => false
            ]);
    }
}
