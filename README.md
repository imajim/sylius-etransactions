# Sylius E-transactions

module de paiement E-transaction pour Sylius.


## Usage

1. Install this bundle

```bash
composer require imajim/e-transactions:dev-master@dev
```

2. Add this line to config/bundles.php
```bash
Imajim\EtransactionPluginBundle\ImajimEtransactionsPluginBundle::class => ['all' => true]
```

3. Add this line to config/services.yaml

```bash
imports:
    - { resource: "@ImajimEtransactionsPluginBundle/Resources/config/services.yaml" }
```

4. Configure payment method in Sylius Admin Panel


